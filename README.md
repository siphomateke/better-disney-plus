# Disney+ Spoiler Duster

Configurable browser extension to hide spoilers in Disney+ including titles, descriptions, show cover art and episode thumbnails.

![Screenshot of home page banner art and thumbnails blocked](./docs/screenshots/home.png)

![Screenshot of Marvel section showing all thumbnails and episode titles blocked](./docs/screenshots/marvel_home.png)

![Screenshot of movie with background image and description blocked](./docs/screenshots/movie.png)

![Screenshot of series page with episode titles, descriptions and thumbnails blocked, but one hovered title visible](./docs/screenshots/series.png)

Supports hiding the following types of spoilers:

- Banner art
- Background images
- Movie, show, extras, trailer and episode thumbnails
- Episode titles and descriptions
- Movie and show synopses

The extension has many options so you can configure it to only hide what you want.

![Screenshot showing all configurable options](./docs/screenshots/options.png)

## Development

Build for development with hot reloading:

```shell
pnpm install
pnpm start
# if targeting chrome:
pnpm start:chrome
```

Build for production:

```shell
pnpm install
pnpm build
# if targeting chrome:
pnpm build:chrome
```
