module.exports = {
  root: true,
  env: {
    browser: true,
    "vue/setup-compiler-macros": true,
  },
  parserOptions: {
    tsconfigRootDir: __dirname,
    project: ["./tsconfig.json"],
  },
  extends: [
    "plugin:unicorn/recommended",
    "@siphomateke/eslint-config-typescript",
    "@vue/typescript/recommended",
    "plugin:vue/vue3-recommended",
    "plugin:vuejs-accessibility/recommended",
  ],
  settings: {
    "import/resolver": {
      // https://github.com/benmosher/eslint-plugin-import/issues/1396
      node: {},
      alias: {
        map: [["@", "./src/"]],
        extensions: [".ts", ".js", ".vue"],
      },
    },
  },
  rules: {
    "@typescript-eslint/lines-between-class-members": "off",
    // We use some custom errors that can't extend Error so this rule must be disabled.
    "@typescript-eslint/no-throw-literal": "off",
    "max-classes-per-file": "off",
    "@typescript-eslint/strict-boolean-expressions": [
      "error",
      {
        allowString: true,
        allowNumber: true,
        allowNullableObject: true,
        allowNullableBoolean: true,
        allowNullableString: true,
        allowNullableNumber: false,
        allowAny: true,
      },
    ],
    "unicorn/prevent-abbreviations": "off",
    "unicorn/no-null": "off",
    "unicorn/no-array-for-each": "off",
  },
  overrides: [
    {
      files: ["src/**/*.vue"],
      parser: "vue-eslint-parser",
      parserOptions: {
        parser: "@typescript-eslint/parser",
      },
      rules: {
        "@typescript-eslint/explicit-module-boundary-types": "off",
        "unicorn/filename-case": ["error", { case: "pascalCase" }],
        "vue/max-attributes-per-line": ["warn", { singleline: 5 }],
        // We use auto imports so some variables will be undefined
        "no-undef": "off",
        // Allow for virtual imports like `~icons/carbon/user`
        "import/no-unresolved": "off",
        "import/extensions": "off",
      },
    },
  ],
};
