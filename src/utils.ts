export const objKeysExact = Object.keys as <T>(o: T) => (Extract<keyof T, string>)[];

export function isUnknownObject(x: unknown): x is { [key in PropertyKey]: unknown } {
  return x !== null && typeof x === 'object';
}