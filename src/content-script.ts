import { LoremIpsum } from 'lorem-ipsum';
import browser from 'webextension-polyfill';
import { loadOptions, Options } from './options';

const spoilerSelectors = [
  // banner images
  '.hero-card .image-container img:not(.image-overlay)',
  // episode card images like "MoonKnight Episodes"
  '[data-gv2containertype="ShelfContainer"]:not([data-testid="continue-watching"]) > .slick-slider [data-program-type="episode"] .basic-card .image-container img',
  // episode card images in "Continue Watching"
  '[data-testid="continue-watching"] [data-program-type="episode"] .basic-card .image-container img',

  // all cards on home page except episodes
  '[data-gv2containertype="ShelfContainer"] div:not([data-program-type="episode"]) > .basic-card .image-container img',
  // watch list cards
  '[data-gv2containertype="GridContainer"] div:not([data-program-type="episode"]) > .basic-card .image-container img',
  // search result cards
  '[data-gv2containerkey="searchResults"] div:not([data-program-type="episode"]) > .basic-card .image-container img',
  // movies page cards
  '[data-gv2containertype="grid"] div:not([data-program-type="episode"]) > .basic-card .image-container img',

  // #region Media details page
  // background image
  '.details .sc-SFOxd > img',
  // trailer thumbnails
  '[data-gv2containerkey="details_extras"] .basic-card .image-container img',
  // version (IMAX, IMAX Enhanced) thumbnails
  '[data-gv2containerkey="details_versions"] .basic-card .image-container img',
  // episode thumbnails
  '[data-gv2containerkey="details_episodes"] [data-program-type="episode"] .basic-card .image-container img',
  // suggested series or movie card image
  '[data-gv2containerkey="details_suggested"] .basic-card .image-container img',
  // logged out suggested series or movie card image
  '[data-testid="related-shelf"] .basic-card .image-container img',
  // #endregion

  // group watch episode Thumbnail
  '.sc-kDgGX .sc-bdVaJa img',
  // group watch up next background image
  '.sc-jKmXuR > img',
];

const classes = {
  wrapper: 'disney-plus-spoiler-wrapper',
  content: 'disney-plus-spoiler',
  noBlur: 'disney-plus-spoiler-no-blur',
  blur: 'disney-plus-spoiler-blur',
  hasLoremText: 'disney-plus-spoiler-has-lorem-text',
  loremText: 'disney-plus-spoiler-lorem-text',
  label: 'disney-plus-spoiler-label',
  hasImage: 'disney-plus-spoiler-has-image',
  hasImageHover: 'disney-plus-spoiler-has-image-hover',
  option: 'disney-plus-spoiler-option',
};

const originalTextAttribute = 'disney-plus-spoiler-original-text';

function createContentWrapper(el: HTMLElement) {
  const wrapper = document.createElement('div');
  wrapper.classList.add(classes.wrapper);

  // TODO: Fix element being added twice
  // el.parentNode?.insertBefore(wrapper, el);
  // wrapper.append(el);

  el.replaceWith(wrapper);
  wrapper.append(el);

  return wrapper;
}

function unwrapContent(el: HTMLElement) {
  const wrapper = el.parentElement;
  wrapper?.replaceWith(el);
}

function isEpisodeId(id: string) {
  return id.match(/episode-s(\d+)-e(\d+)/);
}

function isEpisodeLabel(label: string) {
  return label.match(/S(\d+): E(\d+) /);
}

function getEpisodeNumber(el: HTMLElement) {
  let episode: string | null = el.dataset.testid || null;
  let episodeDetails = episode ? isEpisodeId(episode) : null;
  let child = el;
  while (!episodeDetails) {
    const parentWithTestId = child.closest<HTMLElement>('[data-testid]');
    if (parentWithTestId && parentWithTestId !== child) {
      episode = parentWithTestId?.getAttribute('data-testid');
      episodeDetails = episode ? isEpisodeId(episode) : null;
      child = parentWithTestId;
    } else {
      break;
    }
  }

  if (!episodeDetails) {
    const card = el.closest('.basic-card');
    if (card) {
      // eslint-disable-next-line unicorn/prefer-spread
      const labels = Array.from(card.querySelectorAll<HTMLElement>('[aria-label]'));
      for (const label of labels) {
        episode = label?.getAttribute('aria-label');
        episodeDetails = episode ? isEpisodeLabel(episode) : null;
        if (episodeDetails) {
          break;
        }
      }
    }
  }

  return {
    season: episodeDetails?.[1],
    episode: episodeDetails?.[2],
  };
}

function isInEpisodeCard(el: HTMLElement) {
  return !!el.closest('[data-program-type="episode"]');
}

function isInContinueWatching(el: HTMLElement) {
  return !!el.closest('[data-testid="continue-watching"]');
}

function isInSeasonShelf(el: HTMLElement) {
  return !!el.closest('[data-gv2containerkey="details_episodes"]');
}

function isImage(el: HTMLElement) {
  return el.nodeName === 'IMG';
}

function isProgressBarPreview(el: HTMLElement) {
  return el.classList.contains('progress-bar-preview-image') && el.style.backgroundImage;
}

function isBanner(el: HTMLElement) {
  return !!el.closest('.hero-card');
}

function isCoverImage(el: HTMLElement) {
  return el.closest('.sc-SFOxd') || el.closest('.sc-jKmXuR');
}

function getElementLabel(el: HTMLElement): string | null {
  if (isBanner(el)) {
    return null;
  }

  if (isInEpisodeCard(el)) {
    if (isInSeasonShelf(el) && !!el.closest('#asset-metadata')) {
      return null;
    }

    const episode = getEpisodeNumber(el);
    if (episode.season && episode.episode) {
      return `S${episode.season}: E${episode.episode}`;
    }
    return null;
  }

  const label = el.closest('[aria-label]')?.getAttribute('aria-label') || null;
  if (label?.toLowerCase().includes('to continue watching')) {
    return null;
  }
  return label;
}

function createSpoilerInfo(label: string): HTMLElement {
  const info = document.createElement('div');
  info.classList.add(classes.label);
  info.textContent = label;
  return info;
}

function shouldBlock(el: HTMLElement, options: Options): boolean {
  if (isImage(el)) {
    if (isBanner(el)) {
      return options.hideBanners;
    }
    if (isCoverImage(el)) {
      return options.hideCoverImages;
    }
    if (isInEpisodeCard(el) && !options.hideThumbnails) {
      if (isInContinueWatching(el) && options.hideEpisodeThumbnails) {
        return el.classList.contains('hover-image');
      }
      return options.hideEpisodeThumbnails;
    }
    return options.hideThumbnails;
  }
  return true;
}

function replaceTextWithLorem(el: HTMLElement) {
  // eslint-disable-next-line unicorn/prefer-spread
  let children = Array.from(el.children).filter(child => child instanceof HTMLElement);
  if (children.length === 0) {
    children = [el];
  }
  children.forEach((child) => {
    if (child instanceof HTMLElement) {
      const text = child.textContent;
      if (text) {
        child.setAttribute(originalTextAttribute, text);
        child.classList.add(classes.loremText);

        const originalWords = text.match(/\w+/g);
        if (originalWords) {
          const lorem = new LoremIpsum({ wordsPerSentence: { min: originalWords.length, max: originalWords.length } });
          const randomText = lorem.generateSentences(1);
          child.textContent = randomText.slice(0, Math.max(0, text.length));
        }
      }
    }
  });
}

function restoreText() {
  const items = document.querySelectorAll(`.${classes.loremText}`);
  items.forEach((item) => {
    const originalText = item.getAttribute(originalTextAttribute);
    if (originalText) {
      item.textContent = originalText;
    }
    item.removeAttribute(originalTextAttribute);
    item.classList.remove(classes.loremText);
  });
}

let controlKeyDown = false;
let mouseTarget: HTMLElement | null = null;
function checkMouse() {
  const target = mouseTarget?.classList.contains(classes.hasImage)
    ? mouseTarget
    : mouseTarget?.closest(`.${classes.hasImage}`);
  if (target) {
    if (controlKeyDown) {
      if (!target.classList.contains(classes.hasImageHover)) {
        target.classList.add(classes.hasImageHover);
      }
    } else if (target.classList.contains(classes.hasImageHover)) {
      target.classList.remove(classes.hasImageHover);
    }
  }
}
document.addEventListener('keydown', (event) => {
  if (event.key === 'Control') {
    controlKeyDown = true;
    checkMouse();
  }
});
document.addEventListener('keyup', (event) => {
  if (event.key === 'Control') {
    controlKeyDown = false;
    checkMouse();
  }
});
document.addEventListener('mousemove', (event) => {
  if (event.target instanceof HTMLElement) {
    mouseTarget = event.target;
    checkMouse();
  }
});

function blockElement(el: HTMLElement, options: Options) {
  el.classList.add(classes.content);

  // wrap content in div so we can easily position the label without JS
  const contentWrapper = createContentWrapper(el);

  let showLabel = true;

  if (isImage(el) && (isInContinueWatching(el) || (isInEpisodeCard(el) && !isInSeasonShelf(el)) || el.classList.contains('hover-image'))) {
    showLabel = false;
  }

  if (isProgressBarPreview(el)) {
    showLabel = false;
  }

  let label: string | null = null;
  if (showLabel) {
    label = getElementLabel(el);
    if (label) {
      const infoElement = createSpoilerInfo(label);
      contentWrapper.append(infoElement);
    }
  }

  if (!isImage(el) && options.mangleText && !label) {
    contentWrapper.classList.add(classes.hasLoremText);

    replaceTextWithLorem(el);
  } else if (options.useBlur) {
    contentWrapper.classList.add(classes.blur);
  } else {
    contentWrapper.classList.add(classes.noBlur);
  }

  if (isImage(el)) {
    // cards, banners and cover images
    let hoverEl = el.closest<HTMLElement>('.basic-card, a');
    if (!hoverEl) {
      // group watch thumbnails aren't in cards or links
      hoverEl = el.closest<HTMLElement>(`.${classes.wrapper}`);
    }
    if (hoverEl) {
      hoverEl.classList.add(classes.hasImage);
      hoverEl.addEventListener('mouseleave', () => {
        if (hoverEl && hoverEl.classList.contains(classes.hasImageHover)) {
          hoverEl.classList.remove(classes.hasImageHover);
        }
      });
    }
  }
}

function processSpoilers(selectors: string[], options: Options) {
  // Select all matching elements that haven't already been blocked
  const selector = selectors.map(s => `${s}:not(.${classes.content})`).join(',');
  document.querySelectorAll<HTMLElement>(selector).forEach((el) => {
    if (shouldBlock(el, options)) {
      blockElement(el, options);
    }
  });
}

let observer: MutationObserver | null;

/** Reset any changes the extension made */
function resetPage() {
  observer?.disconnect();
  observer = null;

  restoreText();

  const spoilers = document.querySelectorAll<HTMLElement>(`.${classes.content}`);
  spoilers.forEach((el) => {
    el.querySelector(classes.label)?.remove();
    unwrapContent(el);
    el.classList.remove(classes.content);
  });

  const spoilerWrappers = document.querySelectorAll<HTMLElement>(`.${classes.wrapper}`);
  spoilerWrappers.forEach((wrapper) => {
    wrapper.querySelector(classes.label)?.remove();
    unwrapContent(<HTMLElement>wrapper.firstElementChild);
  });

  const classNames = Object.values(classes);
  const itemsWithClasses = document.querySelectorAll<HTMLElement>(classNames.map(name => `.${name}`).join(','));
  itemsWithClasses.forEach((item) => {
    item.classList.remove(...classNames);
  });
}

function getOptionClass(option: string) {
  return `${classes.option}__${option}`;
}

function updateOptionClasses(options: Options) {
  Object.keys(options).forEach((option) => {
    document.body.classList.remove(getOptionClass(option));
  });
  Object.keys(options).forEach((option) => {
    if (options[<keyof typeof options>option]) {
      document.body.classList.add(getOptionClass(option));
    }
  });
}

async function init() {
  const options = await loadOptions();

  resetPage();

  if (!options.enableBlocking) {
    return;
  }

  let selectors = Array.from(spoilerSelectors);
  if (options.hideEpisodeTitles) {
    selectors = [
      ...selectors,
      // episode titles
      '[data-program-type="episode"] #asset-metadata h5',
      // series details episode titles
      '[data-gv2containerkey="details_episodes"] #asset-metadata p',
      // series details next episode title
      '[data-gv2containerkey="contentMeta"] .body-copy--headline',
      // video player title
      '.controls__header .subtitle-field',
      // video player up next title
      '[data-gv2containerkey="playerUpNext"] h2',
      // group watch episode title
      '.sc-kDgGX h3:nth-of-type(2)',
      // group watch up next episode title
      '.sc-iysEgW h2:nth-of-type(1)',
    ];
  }

  if (options.hideEpisodeDescriptions) {
    selectors = [
      ...selectors,
      // series details episode descriptions
      '[data-gv2containerkey="details_episodes"] #asset-metadata .metadata',
      // series details next episode description
      '[data-gv2containerkey="contentMeta"] .body-copy--headline + .body-copy--large',
      // video player up next description
      '[data-gv2containerkey="playerUpNext"] p.body-copy--large',
      // group watch episode description
      '.sc-kDgGX h3:nth-of-type(2) + p',
      // group watch up next episode description
      '.sc-iysEgW .episode-description',
    ];
  }

  if (options.hideSummaries) {
    selectors = [
      ...selectors,
      // details summary (not next episode title or description)
      '[data-gv2containerkey="contentMeta"] > .sc-dXLFzO + .body-copy--large:last-child',
      // details summary on partially watched movie
      '[data-gv2containerkey="contentMeta"] > .sc-dXLFzO + .body-copy:not(.body-copy--large) + .body-copy--large:last-child',
      // details tab text
      '.kamVeB p.body-copy--large:nth-child(1)',
      // group watch movie summary
      '.sc-kDgGX .sc-bdVaJa + .body-copy',
    ];
  }

  if (options.hideProgressBarPreview) {
    selectors = [
      ...selectors,
      // preview image shown hovering over playback progress bar
      '.progress-bar-preview-image',
    ]
  }

  processSpoilers(selectors, options);
  updateOptionClasses(options);

  observer = new MutationObserver(
    () => processSpoilers(selectors, options),
  );

  observer.observe(document.body, {
    attributes: true,
    childList: true,
    subtree: true,
  });
}

init();
browser.runtime.onMessage.addListener((message, sender) => {
  if (sender.id === browser.runtime.id) {
    if (message === 'optionsUpdated') {
      init();
    }
  }
});