import { storage } from 'webextension-polyfill'
import { isUnknownObject, objKeysExact } from './utils';

export interface Options {
  enableBlocking: boolean;
  useBlur: boolean;
  hideBanners: boolean;
  hideCoverImages: boolean;
  hideThumbnails: boolean;
  hideEpisodeThumbnails: boolean;
  hideEpisodeTitles: boolean;
  hideEpisodeDescriptions: boolean;
  hideSummaries: boolean;
  hideProgressBarPreview: boolean;
  mangleText: boolean;
}

export function getDefaultOptions(): Options {
  return {
    enableBlocking: true,
    useBlur: true,
    hideBanners: true,
    hideCoverImages: true,
    hideThumbnails: true,
    hideEpisodeThumbnails: true,
    hideEpisodeTitles: true,
    hideEpisodeDescriptions: true,
    hideSummaries: true,
    hideProgressBarPreview: false,
    mangleText: false,
  };
};

const optionKeys = Object.keys(getDefaultOptions());

function isOptionsObject(obj: unknown): obj is Options {
  if (isUnknownObject(obj)) {
    for (const key of objKeysExact(obj)) {
      if (!optionKeys.includes(key) || typeof obj[key] !== 'boolean') {
        return false;
      }
    }
    return true;
  }
  return false;
}

export async function loadOptions(): Promise<Options> {
  const loadedOptions = await storage.local.get(optionKeys);
  const defaultOptions = getDefaultOptions();
  if (isOptionsObject(loadedOptions)) {
    return { ...defaultOptions, ...loadedOptions };
  }
  return defaultOptions;
}

export function saveOptions(options: Options) {
  return storage.local.set(options);
}

export function setOptions(options: Partial<Options>) {
  return storage.local.set(options);
}